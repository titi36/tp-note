/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import tpnote.controllers.LoginController;
import tpnote.model.LoginContexte;

/**
 *
 * @author etienne
 */
public class TpNote extends Application {

    private static String mainView_fileName = "/tpnote/views/Login.fxml";

    @Override
    public void start(Stage primaryStage) {


        try {

            // Charge la vue 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TpNote.class.getResource(mainView_fileName));
            BorderPane root = (BorderPane) loader.load();

            // Récupère la référence vers le controller
            LoginController loginCtrler = loader.getController();
            LoginContexte loginCtx = new LoginContexte();
            loginCtrler.setLoginContexte(loginCtx);

            // Créer la nouvelle scene à partir de la vue
            Scene scene = new Scene(root);
            primaryStage.setTitle("login");
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException ex) {
            Logger.getLogger(TpNote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
