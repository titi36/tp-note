/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

/**
 *
 * @author etienne
 */
public class AvatarContext {

    private Personnes personnes;
    private Personne connectedUser;

    public AvatarContext(Personne p, Personnes personnes) {
        this.connectedUser = p;
        this.personnes = personnes;
    }

    public Personne getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(Personne p) {
        this.connectedUser = p;
    }

    public Personnes getPersonnes() {
        return personnes;
    }

    public void setPersonnes(Personnes personnes) {
        this.personnes = personnes;
    }
    
    
}
