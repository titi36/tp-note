/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author etienne
 */
public class Caracteristiques{
    
    public static ObservableList<String> couleurs = FXCollections.observableArrayList("blue","red","green","black");
    
    public static ObservableList<String> longueurs = FXCollections.observableArrayList("court","moyen","grand");
    
    public static ObservableList<String> formes = FXCollections.observableArrayList("rond","ovale","carré");

    
}
