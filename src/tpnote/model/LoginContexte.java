/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpnote.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author etienne
 */
public class LoginContexte {
    
    private Personnes personnes; 
    private StringProperty loginUtilisateurConnecte;
    private StringProperty passwordutilisateurConnecte;

    public LoginContexte() {
        
        // Création de fausses données
        personnes = new Personnes();
        loginUtilisateurConnecte = new SimpleStringProperty();
        passwordutilisateurConnecte = new SimpleStringProperty();
    }

    public Personnes getPersonnes() {
        return personnes;
    }

    public StringProperty getPasswordUtilisateurConnecteProperty() {
        return passwordutilisateurConnecte;
    }
    
    public String getPasswordUtilisateurConnecte() {
        return passwordutilisateurConnecte.getValue();
    }
    
    public StringProperty getLoginUtilisateurConnecteProperty() {
        return loginUtilisateurConnecte;
    }
    
    public String getLoginUtilisateurConnecte() {
        return loginUtilisateurConnecte.getValue();
    }
    
    public Personne identification()
    {
        return personnes.identification(getLoginUtilisateurConnecte(), getPasswordUtilisateurConnecte());
    }
}
